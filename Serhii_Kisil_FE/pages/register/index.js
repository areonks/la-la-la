import {useRouter} from "next/router";
import MainBar from "../../components/bars/mainBar";
import RegisterForm from "../../components/forms/registerForm";
import {toast, ToastContainer} from "react-toastify";
import withoutAuth from '../../hof/withoutAuth';
import {useDispatch} from "react-redux";
import {ErrorList} from "../../components/errors/errorList";
import {register} from "../../store/auth/actions";
import {getProfile} from "../../store/profile/action";

export default function Register() {
    const router = useRouter();
    const dispatch = useDispatch();

    async function handleRegister(userData) {
        try {
            await dispatch(register(userData));
            const  {data: user} = await dispatch(getProfile())
            await router.push(`/users/${user.username}`);
        } catch (error) {
            if (error.response.status === 500) {
                toast.error('Internal Server Error')
            } else toast.error(<ErrorList errors={error.response.data.errors}/>);
        }
    }


    return (
        <div className='d-flex vh-100'>
            <div className='m-auto '>
                <ToastContainer/>
                <MainBar/>
                <div className="container mt-3">
                    <RegisterForm onSubmit={handleRegister} buttonName="Register"/>
                </div>
            </div>

        </div>

    )
}

export const getServerSideProps = withoutAuth();