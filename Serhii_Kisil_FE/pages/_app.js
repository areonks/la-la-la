import '../styles/globals.css'
import {apiClient} from "../libs/apiClient";
import Cookies from 'js-cookie';
import {Provider} from "react-redux";
import {useStore} from "../store";
import 'react-toastify/dist/ReactToastify.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {ToastContainer} from "react-toastify";
import WithPusher from "../components/hoc/withPusher";

export default function MyApp({Component, pageProps}) {
    if (typeof window !== 'undefined') {
        const token = Cookies.get('token');
        apiClient.setToken(token);
    }
    const {store} = useStore(pageProps.initialReduxState)

    return <Provider store={store}>
        <WithPusher>
            <Component {...pageProps} />
        </WithPusher>
        <ToastContainer/>
    </Provider>
}
