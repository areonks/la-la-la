import MainBar from "../../components/bars/mainBar";
import {useRouter} from "next/router";
import LoginForm from "../../components/forms/loginForm";
import withoutAuth from "../../hof/withoutAuth";
import {toast, ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {useDispatch} from "react-redux";
import {login} from "../../store/auth/actions";
import {getProfile} from "../../store/profile/action";

export default function Login() {

    const router = useRouter();
    const dispatch = useDispatch();

    async function handleLogin(credentials) {
        try {
            await dispatch(login(credentials))
            const {data: user} = await dispatch(getProfile())
            await router.push(`/users/${user.username}`);
        } catch (error) {
            toast.error('Email or Password is wrong');
        }
    }

    return (
        <div className='d-flex vh-100'>

            <div className='m-auto '>
                <MainBar/>
                <div className="container mt-3">
                    <LoginForm onSubmit={handleLogin} buttonName="Login"/>
                </div>
            </div>


        </div>

    )
}
export const getServerSideProps = withoutAuth();