import HeaderNavBar from "../../../components/bars/headerNavBar";
import {PostsList} from "../../../components/lists/postsList";
import {withRedux} from "../../../hof/withRedux";
import withAuth from "../../../hof/withAuth";
import {getTaggedPostList} from "../../../store/posts/action";
import {GET_POSTS_LIST} from "../../../store/constants";


export default function TaggedPosts({tag}) {

    return (
        <div className="container">
            <HeaderNavBar/>
            <PostsList tag={tag} queryType={GET_POSTS_LIST}/>
        </div>

    )
}

export const getServerSideProps = withRedux(withAuth(async (ctx, dispatch) => {
        await dispatch(getTaggedPostList(ctx.params.tag))
        return {
            props: {tag: ctx.params.tag}
        }

    }
))
