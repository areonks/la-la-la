import {withRedux} from "../../hof/withRedux";
import withAuth from "../../hof/withAuth";
import {useDispatch} from "react-redux";
import {GET_PROFILE} from "../../store/constants";
import HeaderNavBar from "../../components/bars/headerNavBar";
import ProfileForm from "../../components/forms/profileForm";
import {updateProfile} from "../../store/profile/action";
import {useQuery} from "@redux-requests/react";

export default function Settings() {
    const dispatch = useDispatch()
    const handleUpdateUser = (profile) => {
        dispatch(updateProfile(profile))
    }
    const {data: profile} = useQuery({type: GET_PROFILE});

    return (
        <div className="container">
            <HeaderNavBar/>
            <div className=" m-3">
                <ProfileForm onSubmit={handleUpdateUser} name={profile.name}/>
            </div>
        </div>
    )
}

export const getServerSideProps = withRedux(withAuth(async (ctx, dispatch) => {

        return {
            props: {}
        }
    }
))

