import {withRedux} from "../../hof/withRedux";
import {PostsList} from "../../components/lists/postsList";
import {GET_FOLLOWINGS_POSTS} from "../../store/constants";
import withAuth from "../../hof/withAuth";
import {getFollowingsPosts} from "../../store/posts/action";
import HeaderNavBar from "../../components/bars/headerNavBar";

export default function TaggedPosts() {
    return (
        <div className="container">
            <HeaderNavBar/>
            <PostsList queryType={GET_FOLLOWINGS_POSTS} homePage/>
        </div>

    )
}

export const getServerSideProps = withRedux(withAuth(async (ctx, dispatch) => {

        await dispatch(getFollowingsPosts())
        return {
            props: {}
        }

    }
))
