import {useRouter} from "next/router";
import Link from "next/link";
import {useDispatch} from "react-redux";
import CommentForm from "../../components/forms/commentForm";
import {toast, ToastContainer} from "react-toastify";
import {createCommentAsync, deleteCommentAsync, getPostComments, updateCommentAsync} from "../../store/comments/action";
import {withRedux} from "../../hof/withRedux";
import withAuth from "../../hof/withAuth";
import {Confirm} from "../../helpers/confirmAction";
import {CommentsList} from "../../components/lists/commentsList";
import {Post} from "../../components/items/post";
import {GET_CURRENT_POST, GET_PROFILE} from "../../store/constants";
import {deletePost, getCurrentPost, updatePost} from "../../store/posts/action";
import {useEffect, useRef, useState} from "react";
import {useQuery} from "@redux-requests/react";

export default function PostEdit({postId}) {

    const router = useRouter();
    const dispatch = useDispatch();
    const {data: profile} = useQuery({type: GET_PROFILE});
    const {data: {data: currentPost}} = useQuery({type: GET_CURRENT_POST, multiple: true, requestKey: postId});

    const [commentModel, setCommentModel] = useState({
        id: 0,
        content: '',
        author: profile,
        updateComment: false,
    });

    const handleUpdatePost = async (post) => {
        await dispatch(updatePost(currentPost.id, post));
    }

    const isFirstRender = useRef(true);
    useEffect(() => {
        if (isFirstRender.current) {
            isFirstRender.current = false;
            return;
        }
        if (commentModel.updateComment) {
            dispatch(updateCommentAsync(commentModel.id, commentModel, currentPost.id));
        } else {
            dispatch(createCommentAsync(commentModel, currentPost.id, commentModel.id));
        }
    }, [commentModel])

    const handleDeletePost = async () => {
        toast(<Confirm onYes={async () => {
            try {
                await dispatch(deletePost(currentPost.id));
                await router.push(`/users/${profile.username}`);
            } catch (error) {
                console.log(error)
            }
        }}/>,)
    }

    const handleSubmitComment = (comment) => {
        setCommentModel(prevState => {
            return {
                ...prevState,
                id: comment.id ?? prevState.id + 1,
                ...comment,
                updateComment: !!comment.id,
            }
        })
    }

    const handleDeleteComment = (comment) => {
        dispatch(deleteCommentAsync(comment, currentPost.id))
    }

    return (
        <div className="container">
            <Link href={{pathname: `/users/${profile.username}`}}>
                <a className="float-end btn-link">Home</a>
            </Link>
            <div key={currentPost.id} className="container text-center">
                <Post onUpdate={handleUpdatePost} post={currentPost}
                      showControls={false} onDelete={handleDeletePost}/>
            </div>
            <div className="container">
                <CommentForm onSubmit={handleSubmitComment}/>
            </div>
            <CommentsList onDeleteComment={handleDeleteComment} onUpdate={handleSubmitComment}
                          currentPostId={currentPost.id}/>
        </div>

    )

}

export const getServerSideProps = withRedux(withAuth(async (ctx, dispatch) => {
    try {
        const postId = ctx.params.id
        await dispatch(getPostComments(postId))
        await dispatch(getCurrentPost(postId))

        return {
            props: {postId}
        }
    } catch (e) {
        return {
            notFound: true
        }
    }
}))
