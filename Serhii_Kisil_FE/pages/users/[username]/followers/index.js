import withAuth from "../../../../hof/withAuth";
import {withRedux} from "../../../../hof/withRedux";
import {UsersList} from "../../../../components/lists/usersList";
import {GET_FOLLOWERS, GET_USER} from "../../../../store/constants";
import {useQuery} from "@redux-requests/react";
import {getUser} from "../../../../store/users/action";
import MainLayout from "../../../../components/layout/mainLayout";

export default function Followers() {

    const {data: user} = useQuery({type: GET_USER, multiple: true})

    const {data: {data: followers}, loading} = useQuery({
        type: GET_FOLLOWERS,
        multiple: true,
        requestKey: user.id
    })
    return (
        <div className="container">
            <MainLayout/>
            {!loading
            && Array.isArray(followers)
            && <UsersList queryType={GET_FOLLOWERS} requestKey={user.id}/>
            }
        </div>
    )
}

export const getServerSideProps = withRedux(withAuth(async (ctx, dispatch) => {
        await dispatch(getUser(ctx.params.username))
        return {
            props: {}
        }
    }
))
