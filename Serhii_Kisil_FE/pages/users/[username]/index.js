import PostForm from "../../../components/forms/postForm";
import {useDispatch} from "react-redux";
import withAuth from "../../../hof/withAuth";
import {withRedux} from "../../../hof/withRedux";
import {ToastContainer} from "react-toastify";
import {PostsList} from "../../../components/lists/postsList";
import {createPost, getPostList} from "../../../store/posts/action";
import {GET_POSTS_LIST, GET_PROFILE, GET_USER} from "../../../store/constants";
import {getUser} from "../../../store/users/action";
import {useQuery} from "@redux-requests/react";
import {Follow} from "../../../components/buttons/follow";
import MainLayout from "../../../components/layout/mainLayout";

export default function User() {
    const dispatch = useDispatch();
    const {data: profile} = useQuery({type: GET_PROFILE})
    const {data: user} = useQuery({type: GET_USER, multiple: true})
    const foreignPage = profile.username === user.username

    const handleCreatePost = async (post) => {
        await dispatch(createPost(post, ''));
    }

    return (
        <div className="container">
            <MainLayout/>
            {foreignPage ?
                <div>
                    <PostForm submitButtonName="create post" onSubmit={handleCreatePost}/>
                </div>
                : <Follow/>
            }
            <PostsList queryType={GET_POSTS_LIST}/>
        </div>

    )
}

export const getServerSideProps = withRedux(withAuth(async (ctx, dispatch) => {
        await dispatch(getUser(ctx.params.username));
        await dispatch(getPostList(ctx.params.username))
        return {
            props: {}
        }

    }
))
