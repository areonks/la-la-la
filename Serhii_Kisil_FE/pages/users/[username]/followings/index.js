import withAuth from "../../../../hof/withAuth";
import {withRedux} from "../../../../hof/withRedux";
import {UsersList} from "../../../../components/lists/usersList";
import {GET_FOLLOWED_USERS, GET_USER} from "../../../../store/constants";
import {useQuery} from "@redux-requests/react";
import {getUser} from "../../../../store/users/action";
import MainLayout from "../../../../components/layout/mainLayout";

export default function Followings() {

    const {data: user} = useQuery({type: GET_USER, multiple: true})

    const {data: {data: followedUsers}, loading} = useQuery({
        type: GET_FOLLOWED_USERS,
        multiple: true,
        requestKey: user.id
    })
    return (
        <div className="container">
            <MainLayout/>
            {!loading
            && Array.isArray(followedUsers)
            && <UsersList queryType={GET_FOLLOWED_USERS} requestKey={user.id}/>
            }
        </div>
    )
}

export const getServerSideProps = withRedux(withAuth(async (ctx, dispatch) => {
        await dispatch(getUser(ctx.params.username))
        return {
            props: {}
        }
    }
))
