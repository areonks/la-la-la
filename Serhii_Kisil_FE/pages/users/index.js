import {withRedux} from "../../hof/withRedux";
import withAuth from "../../hof/withAuth";
import HeaderNavBar from "../../components/bars/headerNavBar";
import {getSearchedUsers} from "../../store/users/action";
import {UsersList} from "../../components/lists/usersList";
import ReactPaginate from 'react-paginate';
import {useState} from "react";
import {SEARCH_USERS} from "../../store/constants";
import {useQuery} from "@redux-requests/react";
import {ToastContainer} from "react-toastify";
import ReduxRequestResponseHandler from "../../components/errors/reduxRequestResponseHandler";

export default function UsersSearch() {
    const [searchParams, setSearchParams] = useState(['', ''])
    const requestKey = searchParams[0] + searchParams[1]
    const {data: {lastPage: lastPageNumber}} = useQuery({
        type: SEARCH_USERS,
        requestKey: searchParams[0] + searchParams[1],
        action: getSearchedUsers,
        autoLoad: true,
        variables: [...searchParams, true],
        multiple: true
    });

    const handleUserSearch = (event) => {
        setSearchParams(['', event.target.value])
    }

    const handlePageChange = (page) => {
        setSearchParams(prevValues => [page.selected + 1, prevValues[1]])
    }

    return (
        <div className="container min-vh-100">
            <HeaderNavBar/>
         <div>
             <input onChange={(e) => {
                 setTimeout(handleUserSearch, 1000, e)
             }}/>
         </div>
            <ReduxRequestResponseHandler Component={<UsersList requestKey={requestKey} queryType={SEARCH_USERS}/>}
                                         useQueryKey={requestKey} useQueryType={SEARCH_USERS}/>
            <div className="d-flex pagination-container">
                <ReactPaginate pageCount={lastPageNumber} pageRangeDisplayed={5} marginPagesDisplayed={2}
                               pageClassName="page-item " onPageChange={handlePageChange}
                               pageLinkClassName="page-link" containerClassName="pagination"
                               previousClassName="page-item " breakClassName="page-item "
                               nextClassName="page-item " previousLinkClassName="page-link"
                               nextLinkClassName="page-link" breakLinkClassName="page-link"
                />
            </div>
            <ToastContainer/>
        </div>
    )
}

export const getServerSideProps = withRedux(withAuth(async (ctx, dispatch) => {
        await dispatch(getSearchedUsers())

        return {
            props: {}
        }
    }
))

