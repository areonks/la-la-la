import {useQuery} from "@redux-requests/react";
import {GET_PROFILE} from "../../store/constants";
import Pusher from "pusher-js";
import {toast} from "react-toastify";

const pusher = new Pusher(process.env.PUSHER_KEY, {
    cluster: 'eu',
});

export default function WithPusher({children}) {
    const {data: user} = useQuery({type: GET_PROFILE})

    if (!pusher.connection.socket_id && user) {
        const channel = pusher.subscribe(user.username);

        channel.bind('mentionEvent', function (data) {
            const id = data.item?.post_id ? data.item?.post_id : data.item?.id
            toast(<div>your was mentioned <a href={`/posts/${id}`}>here</a></div>)
        });
    }
    return children
}
