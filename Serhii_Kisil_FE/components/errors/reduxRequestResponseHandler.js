import {toast} from "react-toastify";
import {ErrorList} from "./errorList";
import {useQuery} from "@redux-requests/react";
import {Spiner} from "../items/spiner";

export default function ReduxRequestResponseHandler({Component, useQueryType = '', useQueryKey = ''}) {
    const data = useQuery({type: useQueryType, requestKey: useQueryKey})

    if (data.loading) return <Spiner/>

    if (data.error) return toast(<ErrorList errors={[error.message]}/>)

    if (data.data && !data.data.data[0]) return <div>No such user</div>

    if (data.data) return Component

    else return <div>No such user</div>

}