import {Comment} from "../items/comment";
import {GET_COMMENTS_LIST, GET_PROFILE} from "../../store/constants";
import {useQuery} from "@redux-requests/react";
import Like from "../buttons/like";

export function CommentsList({onDeleteComment, onUpdate, currentPostId}) {

    const {data: profile} = useQuery({type: GET_PROFILE});
    const {data: {data: comments}} = useQuery({type: GET_COMMENTS_LIST, requestKey: currentPostId});

    return (
        <ul className="list-group list-group-flush">
            {comments.map(comment => (
                <li className="container  text-lg-center list-group-item" key={comment.id}>
                    <Comment onDelete={onDeleteComment} comment={comment} onUpdate={onUpdate}
                             showControls={profile.username === comment.author.username}/>
                    <div className="display_block float-end mt-1">
                        <Like item={comment} postId={currentPostId}/>

                    </div>
                </li>
            ))}
            {!comments[0]
            && (<div className="container  text-lg-center list-group-item">
                Don`t you wanna be the first? Click Submit!
            </div>)
            }
        </ul>
    )
}
