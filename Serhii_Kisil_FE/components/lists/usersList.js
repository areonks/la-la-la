import Link from "next/link";
import {Avatar} from "../items/avatar";
import {useQuery} from "@redux-requests/react";

export function UsersList({requestKey = '', queryType}) {

    const {data: {data: users}} = useQuery({
        type: queryType,
        requestKey: String(requestKey),
        multiple: true
    })

    return (
        <ul className="list-group list-group-flush w-50 m-auto">
            {users.map((user) => (
                <li className="container  text-lg-center list-group-item" key={user.username}>
                    <Link href={{pathname: `/users/${user.username}`}}>
                        <a className="display_block  mt-1">
                            <div className="m-auto d-flex flex-column">
                                <h2 className="">{user.name} li</h2>
                                <span>@{user.username}</span>
                                <Avatar avatarUrl={user.avatar_url} styleClasses="profile-image m-auto"/>
                            </div>
                        </a>
                    </Link>
                </li>
            ))}
        </ul>
    )
}
