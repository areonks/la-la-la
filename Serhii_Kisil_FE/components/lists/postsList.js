import Link from "next/link";
import {useDispatch, useSelector} from "react-redux";
import {Post} from "../items/post";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faComments} from "@fortawesome/free-solid-svg-icons";
import {useEffect, useState} from "react";
import {GET_PROFILE, GET_USER} from "../../store/constants";
import {useQuery} from "@redux-requests/react";
import {getFollowingsPosts, getPostList, getTaggedPostList} from "../../store/posts/action";
import {getAllPosts} from "../../store/posts/selectors";
import {Spiner} from "../items/spiner";
import Like from "../buttons/like";

export function PostsList({onScrollEnd, tag, queryType, homePage}) {
    const dispatch = useDispatch();
    const {data: profile} = useQuery({type: GET_PROFILE});
    const {data: user} = useQuery({type: GET_USER, multiple: true});
    const [paginationCursorState, setPaginationCursorState] = useState([''])
    const {posts, loading, paginationCursor} = useSelector(state => getAllPosts(state, queryType));

    const loadMore = () => {
        setPaginationCursorState(prevState => [...prevState, paginationCursor])
        if (tag) {
            return dispatch(getTaggedPostList(tag, paginationCursor));
        }
        if (homePage) {
            return dispatch(getFollowingsPosts(paginationCursor));
        }
        dispatch(getPostList(user.username, paginationCursor));
    }

    useEffect(() => {
        if (document.documentElement.offsetHeight < window.innerHeight
            && !loading
            && !!paginationCursor) {
            loadMore()
        }
        const infiniteScroll = () => {
            if (window.innerHeight + window.pageYOffset === document.documentElement.offsetHeight
                && !loading
                && !!paginationCursor
            ) {
                if (onScrollEnd) onScrollEnd();
                loadMore();
            }
        }
        window.addEventListener('scroll', infiniteScroll);
        return () => window.removeEventListener('scroll', infiniteScroll);
    }, [loadMore]);

    return (
        <ul className="list-group list-group-flush">
            {posts.map(post => (
                <li className="container  text-lg-center list-group-item" key={post.id}>
                    <Post post={post} requestKeys={paginationCursorState}
                          showControls={profile.username === user.username}/>
                    <div className="display_block float-end mt-1">
                        <Like item={post} postMutationRequestKeys={paginationCursorState}/>
                        <Link href={{pathname: `/posts/${post.id}`}}>
                            <a className="btn btn-primary"><FontAwesomeIcon icon={faComments}/></a>
                        </Link>
                    </div>
                </li>
            ))}
            {loading && (<Spiner/>)}
        </ul>
    )
}
