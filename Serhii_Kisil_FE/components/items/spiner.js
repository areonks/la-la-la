export function Spiner() {
    return (
        <div className="spinner-border m-auto p-4 mb-5 mt-5" role="status">
            <span className="visually-hidden">Loading...</span>
        </div>
    )
}