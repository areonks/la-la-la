import CommentForm from "../forms/commentForm";
import {useState} from "react";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faPen, faTrash} from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import {Avatar} from "./avatar";
import {DynamicContent} from "./dynamicContent";

export function Comment({comment, showControls, onUpdate, onDelete}) {

    const [editing, setEditing] = useState(false)

    const handleUpdate = (updatedContent) => {
        setEditing(!editing)
        onUpdate({id: comment.id, ...updatedContent})
    }

    const content = editing ? <CommentForm oldContent={comment.content} onSubmit={handleUpdate}/> :
        <DynamicContent content={comment.content} mentions={comment.mentionedUsers} tags={comment.tags}/>


    const controls = showControls ? (
        <div className="float-end ">
            <button onClick={() => setEditing(!editing)} className="btn btn-primary m-1"><FontAwesomeIcon icon={faPen}/>
            </button>
            <button onClick={() => onDelete(comment)} className="btn btn-danger"><FontAwesomeIcon icon={faTrash}/>
            </button>
        </div>
    ) : null

    return (<div className="flex-row">
            <div className="w-50 m-auto">
                <div className="fit-content">
                    <Avatar styleClasses="profile-image-s d-inline"
                            avatarUrl={comment.author.avatar_url}/>
                    <Link href={{pathname: `/users/${comment.author.username}`}}>
                        <a className="font-weight-bold d-inline"> {comment.author.name} </a>
                    </Link>
                </div>
            </div>

            {content}
            {controls}
        </div>

    )
}
