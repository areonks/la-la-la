export function Avatar({avatarUrl, styleClasses}) {
    const imageUrl =  avatarUrl ? avatarUrl : "../defaultLogo.png"

    return (
        <img src={imageUrl} className={styleClasses}/>
    )
}