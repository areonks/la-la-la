import reactStringReplace from "react-string-replace";

export function DynamicContent({content = '', mentions = [], tags = []}) {

    content = reactStringReplace(content, /@([a-z\d_\.]+)/ig, (match, i) => {
        const mentioned = mentions.find(element => element.username === match)
        if (!mentioned) return '@' + match
        if (mentioned) {
            return (
                <a key={match + i} href={`/users/${mentioned.username}`}>@{mentioned.name}</a>
            )
        }
    });
    content = reactStringReplace(content, /#(\w+)/g, (match, i) => {
        const tag = tags.find(element => element === match)
        if (!tag) return '#' + match
        if (tag) {
            return (
                <a key={match + i} href={`/tags/${match}/posts`}>#{match}</a>
            )
        }
    });
    return <div className="container w-50">
        <p className="text-left">{content}</ p>
    </div>
}