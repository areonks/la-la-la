import {useState} from "react";
import PostForm from "../forms/postForm";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPen, faTrash} from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import {DynamicContent} from "./dynamicContent";
import {GET_POSTS_LIST} from "../../store/constants";
import {useQuery} from "@redux-requests/react";
import {deletePost, updatePost} from "../../store/posts/action";
import {useDispatch} from "react-redux";

export function Post({post, showControls, requestKeys}) {
    const dispatch = useDispatch()
    const [editing, setEditing] = useState(false)
    const {data: {tagName}} = useQuery({type: GET_POSTS_LIST, multiple: true});

    const handleUpdate = (updatedContent) => {
        setEditing(false)
        dispatch(updatePost(post.id, updatedContent, requestKeys))
    }
    const onDelete = async (postId) => {
        await dispatch(deletePost(postId, requestKeys));
    }
    const content = editing ?
        <PostForm oldContent={post.content} onSubmit={handleUpdate}/> :
        <DynamicContent content={post.content} mentions={post.mentionedUsers} tags={post.tags}/>

    const controls = showControls ? (
        <div className="float-end ">
            <button onClick={() => setEditing(!editing)} className="btn btn-primary m-1"><FontAwesomeIcon icon={faPen}/>
            </button>
            <button onClick={() => onDelete(post.id)} className="btn btn-danger"><FontAwesomeIcon icon={faTrash}/>
            </button>
        </div>
    ) : null

    const author = !tagName ? <div className="font-weight-bold">{post.author.name} </div> :
        <Link className="font-weight-bold" href={'/users/' + post.author.username}>
            <a>{post.author.name}</a>
        </Link>
    return (<div className="container">
            {author}
            {content}
            {controls}
        </div>

    )
}
