import {Logout} from "../buttons/logout";
import HeaderNavBar from "../bars/headerNavBar";
import UserBar from "../bars/userBar";
import {FollowersBar} from "../bars/followersBar";
import {ToastContainer} from "react-toastify";

export default function MainLayout (){

    return(
        <div>
            <Logout/>
            <HeaderNavBar/>
            <UserBar/>
            <FollowersBar/>

        </div>
    )
}