import {ErrorMessage, Field, Form, Formik} from 'formik';
import * as Yup from 'yup'

export default function LoginForm({onSubmit, buttonName}) {

    const validationSchema = Yup.object().shape({
        email: Yup.string().email('Wrong email format').required('Email required'),
        password: Yup.string().required('Password required')


    })
    return (
        <Formik validateOnChange={false} initialValues={{
            email: '',
            password: ''
        }}
                validationSchema={validationSchema}
                onSubmit={onSubmit}>

            <Form className="container">
                <div className="col justify-content-md-center">
                    <div className="form-floating mb-3">

                        <Field type="email" className="form-control"  placeholder='name@example.com'
                               name="email"/>
                        <label htmlFor="email">Email</label>
                        <ErrorMessage
                            name="email"
                            component="div"
                            className="field-error"
                        />
                    </div>

                    <div className=" form-floating mb-3">
                        <Field className='form-control' name="password" placeholder='Password' type='password'/>
                        <label htmlFor="password">Password</label>
                        <ErrorMessage
                            name="password"
                            component="div"
                            className="field-error"
                        />
                    </div>
                </div>
                <div className="row justify-content-md-center">
                    <button className="btn btn-primary" type='submit' name="submit">{buttonName}</button>
                </div>
            </Form>
        </Formik>
    )
}