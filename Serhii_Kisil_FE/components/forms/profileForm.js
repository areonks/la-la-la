import {ErrorMessage, Formik} from 'formik';
import * as Yup from 'yup'
import {Avatar} from "../items/avatar";
import {GET_PROFILE} from "../../store/constants";
import {useQuery} from "@redux-requests/react";


export default function ProfileForm({name, onSubmit}) {

    const {data: profile} = useQuery({type: GET_PROFILE});

    const validationSchema = Yup.object().shape({
        name: Yup.string().required('Content is required'),
        avatar: Yup.mixed().test('fileSize', "File Size is too large", value => value ? value.size <= 2048000 : true)
            .test('fileType', "Unsupported File Format", value => value ? ['image/png', 'image/jpeg'].includes(value?.type) : true)
    });
    return (
        <Formik validateOnBlur={false} validateOnChange={false} initialValues={{
            name: name || '',
            avatar: '',
        }}
                enableReinitialize={true}
                validationSchema={validationSchema}
                onSubmit={(values, formikHelpers) => {
                    onSubmit(values);
                    formikHelpers.resetForm();
                }}>
            {props => (

                <form className="m-auto fit-content" onSubmit={
                    props.handleSubmit
                }>
                    <div className="m-3">
                        <label htmlFor="avatar">
                            {props.values.avatar
                                ? <Avatar avatarUrl={URL.createObjectURL(props.values.avatar)}
                                          styleClasses="border border-4 profile-image"/>
                                : <Avatar avatarUrl={profile.avatar_url}
                                          styleClasses="border border-4 profile-image"/>}
                        </label>
                        <ErrorMessage
                            name="avatar"
                            component="div"
                            className="field-error"
                        />
                        <input type="file" className="d-none" name="avatar" id="avatar" onChange={(event) => {
                            props.setFieldValue("avatar", event.currentTarget.files[0]);
                        }}/>
                    </div>

                    <div>
                        <input name="name" onChange={props.handleChange} value={props.values.name}/>
                    </div>
                    <button className="btn btn-primary m-1" type="submit">Submit</button>
                    <ErrorMessage
                        name="name"
                        component="div"
                        className="field-error"
                    />
                </form>

            )}

        </Formik>
    )
}