import {ErrorMessage, Field, Form, Formik} from "formik";
import * as Yup from 'yup'

export default function RegisterForm({onSubmit, buttonName}) {

    const validationSchema = Yup.object().shape({
        email: Yup.string().email('Wrong email format').required('Email required'),
        username: Yup.string().required('Username required'),
        name: Yup.string().required('Name required'),
        password: Yup.string().required('Password required'),
        password_confirmation: Yup.string().required('Confirm Password please')
            .oneOf([Yup.ref('password'), null], 'Passwords must match')


    })
    return (
        <Formik validateOnChange={false} initialValues={{
            email: '',
            password: '',
            name: '',
            username: '',
            password_confirmation: ''
        }}
                validationSchema={validationSchema}
                onSubmit={onSubmit}>
            <Form className="container ">
                <div className="col justify-content-md-center">
                    <div className=" form-floating mb-3">
                        <Field className="form-control " placeholder='Email' name="email"/>
                        <label htmlFor="email">Email</label>

                        <ErrorMessage
                            name="email"
                            component="div"
                            className="field-error"
                        />
                    </div>
                    <div className="form-floating mb-3">
                        <Field name="username" className="form-control " placeholder='username'/>
                        <label htmlFor="username">Username</label>

                        <ErrorMessage
                            name="username"
                            component="div"
                            className="field-error"
                        />
                    </div>

                    <div className=" form-floating mb-3">
                        <Field name="name" className="form-control " placeholder='Name'/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <ErrorMessage
                        name="name"
                        component="div"
                        className="field-error"
                    />
                    <div className=" form-floating mb-3">
                        <Field name="password" className="form-control " placeholder='Password' type='password'/>
                        <label htmlFor="password">Password</label>

                    </div>
                    <ErrorMessage
                        name="password"
                        component="div"
                        className="field-error"
                    />
                    <div className=" form-floating mb-3">
                        <Field name="password_confirmation" className="form-control " placeholder='Confirm password'
                               type='password'/>
                        <label htmlFor="password_confirmation">Confirm passwor</label>

                        <ErrorMessage
                            name="password_confirmation"
                            component="div"
                            className="field-error"
                        />
                    </div>
                    <div className="row justify-content-md-center">
                        <button className="btn btn-primary" type='submit'>{buttonName}</button>
                    </div>
                </div>
            </Form>
        </Formik>

    )
}