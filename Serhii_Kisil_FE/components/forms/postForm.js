import {ErrorMessage, Field, Form, Formik} from 'formik';
import * as Yup from 'yup'
import {parseAutocomplete} from "../../helpers/mentionAutocomplete";

export default function PostForm({oldContent = '', onSubmit}) {

    const validationSchema = Yup.object().shape({
        content: Yup.string().required('Content is required').max(255, 'Too Big')
    });

    const {checkMentioning, AutocompleteSuggestionsList, content, resetContent} = parseAutocomplete(oldContent)
    return (
        <Formik validateOnBlur={false} validateOnChange={false} initialValues={{
            content: content || '',
        }}
                enableReinitialize={true}
                validationSchema={validationSchema}
                onSubmit={(values, formikHelpers) => {
                    onSubmit(values)
                    formikHelpers.resetForm()
                    resetContent()
                }}>
            <Form className="container w-50">
                <Field as="textarea" name="content" className="form-control" onKeyUp={checkMentioning}/>
                <button className="btn btn-secondary mt-1" type='submit'>Submit</button>
                <ErrorMessage
                    name="content"
                    component="div"
                    className="field-error"
                />
                {AutocompleteSuggestionsList}
            </Form>


        </Formik>
    )
}