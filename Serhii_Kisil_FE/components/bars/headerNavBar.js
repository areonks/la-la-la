import Link from "next/link";
import {GET_POSTS_LIST, GET_PROFILE, GET_USER} from "../../store/constants";
import {useQuery} from "@redux-requests/react";

export default function HeaderNavBar({shallow = false}) {
    const {data: profile} = useQuery({type: GET_PROFILE});

    const {data: user} = useQuery({type: GET_USER, multiple: true})

    const foreignPage = profile.username === user.username

    return (
        <div className="container mt-2">
            <div className="row justify-content-md-center">
                <Link href="/followings/posts" shallow={foreignPage}>
                    <a className="col-md-auto">
                        Home
                    </a>
                </Link>
                <Link href={`/users/${profile.username}`} shallow={foreignPage}>
                    <a className="col-md-auto">Profile</a>
                </Link>
                <Link href="/settings">
                    <a className="col-md-auto">Settings</a>
                </Link>
                <Link href="/users" shallow>
                    <a className="col-md-auto">
                        Find a friend
                    </a>
                </Link>
            </div>


        </div>
    )
}
