import {Avatar} from "../items/avatar";
import {GET_USER} from "../../store/constants";
import {useQuery} from "@redux-requests/react";

export default function UserBar() {
    const {data: user} = useQuery({type: GET_USER, multiple: true});


    return (
        <div className="d-flex m-3">
            <div className="m-auto d-flex flex-column">
                <h2 className="">{user.name} </h2>
                <Avatar avatarUrl={user.avatar_url} styleClasses="profile-image m-auto"/>
            </div>

        </div>
    )
}