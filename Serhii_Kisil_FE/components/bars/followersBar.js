import {useQuery} from "@redux-requests/react";
import {GET_FOLLOWED_USERS, GET_FOLLOWERS, GET_USER} from "../../store/constants";
import {getFollowedUsers, getFollowers} from "../../store/follows/actions";
import Link from "next/link";

export function FollowersBar() {

    const {data: user} = useQuery({type: GET_USER, multiple: true})

    const {data: {data: followers}} = useQuery({
        type: GET_FOLLOWERS,
        action: getFollowers,
        requestKey: user.id,
        variables: [user.id, user.id],
        autoLoad: true,
        multiple: true
    })
    const {data: {data: followedUsers}} = useQuery({
        type: GET_FOLLOWED_USERS,
        action: getFollowedUsers,
        requestKey: user.id,
        variables: [user.id, user.id],
        autoLoad: true,
        multiple: true
    })

    return (<div className="row justify-content-md-center">
            <Link href={`/users/${user.username}/followers`}>
                <a className="col-md-auto">Followers {followers?.length}</a>
            </Link>
            <Link href={`/users/${user.username}/followings`}>
                <a className="col-md-auto">Followed Users {followedUsers?.length}</a>
            </Link>
        </div>
    )
}