import 'bootstrap/dist/css/bootstrap.min.css'
import Link from "next/link";

export default function MainBar() {

    return (
        <div className="container">
            <div className="row justify-content-md-center">
                <Link href="/register">
                    <a className="col-md-auto">Register Page</a>
                </Link>
                <Link href="/login">
                    <a className="col-md-auto">Login Page</a>
                </Link>
            </div>


        </div>
    )
}
