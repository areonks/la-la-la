import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faThumbsUp} from "@fortawesome/free-solid-svg-icons";
import {useDispatch} from "react-redux";
import {likeComment, likePost, unLikeComment, unLikePost} from "../../store/like/actions";

export default function Like({item, postId, postMutationRequestKeys}) {
    const dispatch = useDispatch()

    const addLike = (e) => {
        e.preventDefault()
        if (postId) {
            if (!item.isLiked) {
                return dispatch(likeComment(item.id, postId))
            } else {
                return dispatch(unLikeComment(item.id, postId))
            }
        } else {
            if (!item.isLiked) {
                return dispatch(likePost(item.id, postMutationRequestKeys))
            } else {
                return dispatch(unLikePost(item.id, postMutationRequestKeys))
            }
        }

    }

    return (
        <button onClick={addLike} className="btn btn-primary mx-1"><FontAwesomeIcon icon={faThumbsUp}/>{item.likes}
        </button>

    )

}