import {useQuery} from "@redux-requests/react";
import {GET_FOLLOWED_USERS, GET_PROFILE, GET_USER} from "../../store/constants";
import {follow, getFollowedUsers, unfollow} from "../../store/follows/actions";
import {useDispatch} from "react-redux";
import {useState} from "react";

export function Follow() {
    const dispatch = useDispatch()
    const {data: profile} = useQuery({type: GET_PROFILE, multiple: true})
    const {data: user} = useQuery({type: GET_USER, multiple: true})

    const [key, setKey] = useState(0);

    const {data: {data: followedUsers}} = useQuery({
        type: GET_FOLLOWED_USERS,
        autoLoad: true,
        requestKey: key,
        variables: [profile.id, key],
        action: getFollowedUsers,
        multiple: true
    })
        const isFollowed = followedUsers?.map(followedUser => followedUser.username === user.username).includes(true)
    const handleUnfollow = () => {
        setKey(key + 1)
        dispatch(unfollow(user.id, key))
    }
    const handleFollow = () => {
        setKey(key + 1)
        dispatch(follow(user.id, key))
    }

    return (
        isFollowed ?
            <button onClick={handleUnfollow}>Unfollow</button> :
            <button onClick={handleFollow}>Follow</button>
    )
}