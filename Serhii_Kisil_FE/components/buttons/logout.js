import {useDispatch} from "react-redux";
import {useRouter} from "next/router";
import {toast} from "react-toastify";
import {Confirm} from "../../helpers/confirmAction";
import {logout} from "../../store/auth/actions";

export function Logout() {

    const dispatch = useDispatch();
    const router = useRouter();

    const handleLogout = () => {
        toast.error(<Confirm onYes={async () => {
            try {
                await dispatch(logout());
                await router.push('/login');
            } catch (error) {
                console.log(error)
            }
        }}/>,)
    }
    return (
        <button onClick={handleLogout} className="float-end btn-link">Logout</button>

    )
}