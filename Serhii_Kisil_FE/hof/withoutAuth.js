import {apiClient} from "../libs/apiClient";

const withoutAuth = (getServerSidePropsFunc) => {
    return async (ctx) => {
        const token = ctx.req.cookies.token
        if (token) {
            try {
                apiClient.setToken(token)
                const {data: {data: profile}} = await apiClient.instance('/profile')
                return {
                    redirect: {
                        destination: `/users/${profile.username}`,
                        permanent: false,
                    }
                }
            } catch (e) {
                if (getServerSidePropsFunc) {
                    const result = getServerSidePropsFunc(ctx)
                    return {
                        ...result,
                        props: {
                            ...result.props
                        }
                    }
                }
                return {
                    props: {}
                }
            }
        }
        if (getServerSidePropsFunc) {
            const result = getServerSidePropsFunc(ctx)
            return {
                ...result,
                props: {
                    ...result.props
                }
            }
        }
        return {
            props: {}
        }
    }
}
export default withoutAuth