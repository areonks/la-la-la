import {apiClient} from "../libs/apiClient";
import {getProfile} from "../store/profile/action";

const withAuth = (getServerSidePropsFunc) => {
    return async (ctx, dispatch) => {
        const token = ctx.req.cookies.token
        if (token) {
            try {
                apiClient.setToken(token)
                if (getServerSidePropsFunc) {
                    const {data: {data}} = await dispatch(getProfile())
                    const result = await getServerSidePropsFunc(ctx, dispatch)
                    return {
                        ...result,
                        props: {
                            ...result.props
                        }
                    }
                }
                return {
                    props: {}
                }
            } catch (e) {
                return {
                    redirect: {
                        destination: '/login',
                        permanent: false
                    }
                }
            }
        }
        return {
            redirect: {
                destination: '/login',
                permanent: false,
            }
        }
    }
}
export default withAuth