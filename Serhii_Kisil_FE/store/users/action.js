import {GET_USER, SEARCH_USERS} from "../constants";

export const getSearchedUsers = (pageNumber = '', username = '', cache) => ({
    type: SEARCH_USERS,
    request: {url: `users?username=${username}&page=${pageNumber}`},
    meta: {
        requestKey:pageNumber + username,
        getData: data => ({
            data: data.data,
            lastPage: data.meta ? data.meta.last_page : null
        }),
        requestsCapacity: 5,
        cache: cache,
    }
})

export const getUser = (username) => ({
    type: GET_USER,
    request: {url: `/users/${username}`},
    meta: {
        getData: data => data.data
    },
})

