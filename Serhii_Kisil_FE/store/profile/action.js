import {GET_PROFILE, UPDATE_PROFILE} from "../constants";

export const updateProfile = (updatedProfile) => {
    const formData = new FormData();
    formData.append('name', updatedProfile.name);
    formData.set('_method', 'put');
    if (updatedProfile.avatar) {
        formData.append('avatar', updatedProfile.avatar)
    }

    return {
        type: UPDATE_PROFILE,
        request: {
            url: 'profile',
            method: 'POST',
            data: formData
        },
        meta: {
            mutations: {
                [GET_PROFILE]: {
                    updateData: (data, mutationData) => mutationData.data
                }
            }
        }
    }
}

export const getProfile = () => ({
    type: GET_PROFILE,
    request: {url: `/profile`},
    meta: {
        getData: data => data.data
    }
})
