export const GET_USER = 'GET_USER';
export const SEARCH_USERS = 'SEARCH_USERS';

export const GET_CURRENT_POST = 'GET_CURRENT_POST';
export const GET_POSTS_LIST = 'GET_POSTS_LIST';
export const CREATE_POST = 'CREATE_POST';
export const UPDATE_POST = 'UPDATE_POST';
export const DELETE_POST = 'DELETE_POST';
export const GET_FOLLOWINGS_POSTS = 'GET_FOLLOWINGS_POSTS';

export const GET_COMMENTS_LIST = 'GET_COMMENTS_LIST';
export const CREATE_COMMENT = 'CREATE_COMMENT';
export const UPDATE_COMMENT = 'UPDATE_COMMENT';
export const DELETE_COMMENT = 'DELETE_COMMENT';


export const LOGIN = 'LOGIN';
export const GET_PROFILE = 'GET_PROFILE';
export const UPDATE_PROFILE = 'UPDATE_PROFILE';

export const GET_FOLLOWED_USERS = 'GET_FOLLOWED_USERS';
export const GET_FOLLOWERS = 'GET_FOLLOWERS';
export const FOLLOW = 'FOLLOW';
export const UNFOLLOW = 'UNFOLLOW';

export const LIKE = 'LIKE';
export const UNLIKE = 'UNLIKE';