import {GET_COMMENTS_LIST, LIKE, UNLIKE} from "../constants";
import {postListMutation} from "../posts/statesMutations";

export const likePost = (postId, requestKeys) => ({
    type: LIKE,
    request: {
        url: `posts/${postId}/like`,
        method: 'POST'
    },

    meta: (postListMutation(postId, requestKeys, post => ({...post, likes: post.likes + 1, isLiked: true})))
})
export const unLikePost = (id, requestKeys) => ({
    type: UNLIKE,
    request: {
        url: `posts/${id}/unlike`,
        method: 'POST'
    },
    meta: (postListMutation(id, requestKeys, post => ({...post, likes: post.likes - 1, isLiked: false}))),

})
export const likeComment = (id, mutationKey) => ({
    type: LIKE,
    request: {
        url: `comments/${id}/like`,
        method: 'POST'
    },
    meta: {
        mutations: {
            [GET_COMMENTS_LIST + mutationKey]: {
                updateData: (data) => ({
                    data: data.data.map(comment => comment.id === id ? {
                        ...comment,
                        likes: comment.likes + 1,
                        isLiked: true
                    } : comment)
                    ,
                }),
            }
        },
        requestKey: mutationKey,
    }
})
export const unLikeComment = (id, mutationKey) => ({
    type: UNLIKE,
    request: {
        url: `comments/${id}/unlike`,
        method: 'POST'
    },
    meta: {
        mutations: {
            [GET_COMMENTS_LIST + mutationKey]: {
                updateData: (data) => ({
                    data: data.data.map(comment => comment.id === id ? {
                        ...comment,
                        likes: comment.likes - 1,
                        isLiked: false
                    } : comment)
                    ,
                }),
            }
        },
        requestKey: mutationKey,
    }
})