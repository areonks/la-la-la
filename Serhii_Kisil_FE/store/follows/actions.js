import {FOLLOW, GET_FOLLOWED_USERS, GET_FOLLOWERS, UNFOLLOW} from "../constants";

export const getFollowedUsers = (id, requestKey) => {
    return {
        type: GET_FOLLOWED_USERS,
        request: {url: `users/${id}/followings`},
        meta: {
            requestKey: requestKey,
        }
    }

}


export const getFollowers = (id, requestKey) => ({
    type: GET_FOLLOWERS,
    request: {url: `users/${id}/followers`},
    meta: {
        requestKey: requestKey,
    }
})

export const unfollow = (id, requestKey) => ({
    type: UNFOLLOW,
    request: {
        url: `${id}/unfollow`,
        method: 'POST'
    },
    meta: {
        requestKey: requestKey,
    }
})

export const follow = (id, requestKey) => ({
    type: FOLLOW,
    request: {
        url: `${id}/follow`,
        method: 'POST'
    },
    meta: {
        requestKey: requestKey,
    }
})
