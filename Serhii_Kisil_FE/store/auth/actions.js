import {LOGIN} from "../constants";
import Cookie from "js-cookie";
import {apiClient} from "../../libs/apiClient";

export const logout = () => ({
    type: LOGIN,
    request: {
        url: '/logout',
        method: 'POST',
    },
    meta: {
        onSuccess: () => {
            Cookie.remove('token');
        }
    }

})
export const register = (userData) => ({
    type: LOGIN,
    request: {
        url: '/register',
        method: 'POST',
        data: userData,
    },
    meta: {
        onSuccess: response => {
            Cookie.set('token', response.data.data.token);
            apiClient.setToken(response.data.data.token);
        }
    }

})

export const login = (credentials) => ({
    type: LOGIN,
    request: {
        url: '/login',
        method: 'POST',
        data: credentials,
    },
    meta: {
        onSuccess: response => {
            Cookie.set('token', response.data.data.token);
            apiClient.setToken(response.data.data.token);
        }
    }

})

