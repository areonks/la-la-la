import {GET_POSTS_LIST} from "../constants";

export function postListMutation(postId, requestKeys, newPost) {
    const mutations = requestKeys.reduce((accum, key) => {
        Object.assign(accum, {
            [GET_POSTS_LIST + key]: {
                updateData: (data, mutation) => ({
                    posts: data.posts.map(post => post.id === postId ?
                        newPost(post, mutation) : post),
                    paginationCursor: data.paginationCursor
                })
            },
        })
        return accum
    }, {})
    return {
        mutations: mutations,
        requestKey: postId
    }
}