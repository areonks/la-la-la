import {
    CREATE_POST,
    DELETE_POST,
    GET_CURRENT_POST,
    GET_FOLLOWINGS_POSTS,
    GET_POSTS_LIST,
    UPDATE_POST
} from "../constants";
import {parsePaginationCursor} from "../../helpers/parsePaginationCursor";
import {postListMutation} from "./statesMutations";

export const getPostList = (username, paginationCursor = '',) => {
    return {
        type: GET_POSTS_LIST,
        request: {url: `users/${username}/posts?cursor=${paginationCursor}`},
        meta: {
            requestKey: paginationCursor,
            getData: (data) => ({
                posts: data.data,
                paginationCursor: data.links.next ? parsePaginationCursor(data.links.next) : null,
            }),
        }
    }
}

export const deletePost = (postId, requestKeys) => ({
    type: DELETE_POST,
    request: {
        url: `/posts/${postId}`,
        data: postId,
        method: 'DELETE',
    },
    meta: (() => {
        const mutations = requestKeys.reduce((accum, key) => {
            Object.assign(accum, {
                [GET_POSTS_LIST + key]: {
                    updateData: data => ({
                        posts: data.posts.filter(post => post.id !== postId),
                        paginationCursor: data.paginationCursor
                    })
                }
            })
            return accum
        }, {})
        return {
            mutations: mutations,
            requestKey: postId
        }
    })(),
})

export const createPost = (post, requestKey) => ({
    type: CREATE_POST,
    request: {
        url: '/posts',
        data: post,
        method: 'POST',
    },
    meta: {
        requestKey: post.id,
        mutations: {
            [GET_POSTS_LIST + requestKey]: {
                updateData: (data, mutationData) => ({
                    posts: [mutationData.data, ...data.posts],
                    paginationCursor: data.paginationCursor
                })
            },
        },
    },
})

export const updatePost = (postId, post, requestKeys) => ({
    type: UPDATE_POST,
    request: {
        url: `/posts/${postId}`,
        data: post,
        method: 'PUT',
    },
    meta: (postListMutation(postId, requestKeys, (data, mutation) => (mutation.data))),

})

export const getCurrentPost = (postId) => ({
    type: GET_CURRENT_POST,
    request: {url: `/posts/${postId}`},
    meta: {
        requestKey: postId,
    }
})

export const getTaggedPostList = (tag, paginationCursor = '') => ({
    type: GET_POSTS_LIST,
    request: {url: `tags/${tag}/posts?cursor=${paginationCursor}`},
    meta: {
        requestKey: paginationCursor,
        getData: (data) => ({
            paginationCursor: parsePaginationCursor(data.links.next),
            posts: data.data,
            tagName: tag
        })

    }

})

export const getFollowingsPosts = (paginationCursor = '') => ({
    type: GET_FOLLOWINGS_POSTS,
    request: {url: `users/followings/posts?cursor=${paginationCursor}`},
    meta: {
        requestKey: paginationCursor,
        getData: (data) => ({
            paginationCursor: parsePaginationCursor(data.links.next),
            posts: data.data,
        })

    }
})