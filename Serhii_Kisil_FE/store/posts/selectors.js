export const getAllPosts = (state, queryType) => {
    const requestsKeys = state.requests.requestsKeys[queryType];
    const lastRequestsKeys = requestsKeys[requestsKeys.length - 1];
    const {data, pending} = state.requests.queries[queryType + lastRequestsKeys];
    const posts = requestsKeys.reduce((accum, key) => {
        const request = state.requests.queries[queryType + key];
        if (!request.pending && Array.isArray(request.data?.posts)) {
            accum = accum.concat(request.data.posts);
        }
        return accum;
    }, []);

    return {
        posts,
        paginationCursor: !pending && data?.paginationCursor,
        loading: Boolean(pending)
    };
}