export function parsePaginationCursor(url) {
    if (url) return /cursor=\b(\w+)/g.exec(url)[1]
    else return null
}