import {useDispatch} from "react-redux";
import {useEffect, useState} from "react";
import {useQuery} from "@redux-requests/react";
import {SEARCH_USERS} from "../store/constants";
import {getSearchedUsers} from "../store/users/action";

export function parseAutocomplete(oldContent) {

    const dispatch = useDispatch()

    const [showSuggestions, setShowSuggestions] = useState(false)
    const [searchValue, setSearchValue] = useState('')
    const [content, setContent] = useState(oldContent)
    const {data: {data: suggestedUsers}} = useQuery({type: SEARCH_USERS, requestKey: searchValue, multiple: true})

    const checkMentioning = (e) => {
        const content = e.target.value
        if (content.slice(-1) === ' ') {
            return setShowSuggestions(false)
        }
        if (content.slice(-1) === '@') {
            setShowSuggestions(true)
        }
        setContent(content)
        const parsedContent = content.split(/@([a-z\d_\.]+)/ig)
        setSearchValue(parsedContent[parsedContent.length - 2])
    }

    useEffect(() => {
        dispatch(getSearchedUsers('', searchValue, true))
    }, [searchValue])

    const swapContent = (username) => {
        const parsedContent = content.split(/(@[a-z\d_\.]+)/ig)
        parsedContent[parsedContent.length - 2] = '@' + username
        setContent(parsedContent.join(''))
        setShowSuggestions(false)
    }
    const resetContent = () => {
        setContent('')
    }
    return {
        resetContent,
        content,
        checkMentioning,
        AutocompleteSuggestionsList: (
            showSuggestions && suggestedUsers?.map(user =>
                <div key={user.username} onClick={() => {
                    swapContent(user.username)
                }}>
                    {user.username}
                </div>
            )
        )
    }
}



