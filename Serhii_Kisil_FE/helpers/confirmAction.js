export const Confirm = ({onYes, closeToast}) => {
    const handleClick = () => {
        onYes();
        closeToast();
    };

    return (
        <div>
            <p className="d-inline">
                Are you sure?

            </p> <button className="d-inline btn btn-dark" onClick={handleClick}>Yes</button>
        </div>
    );
}



