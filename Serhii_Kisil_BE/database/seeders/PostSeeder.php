<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::all();
        $user->each(function ($user) {
            $user->posts()->saveMany(
                Post::factory()->count(rand(0, 20))->for($user)->create()
            );
        });
        Post::all()->each(function ($post) use ($user) {
            for ($x = 0; $x < rand(1, 50); $x++)
                $post->like($user->random()->id);
        });
    }
}
