<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::all();
        Post::all()->each(function ($post) use ($user) {
            for ($x = 0; $x < rand(1, 3); $x++) {
                $post->comments()->save(
                    Comment::factory()->for($post)->for($user->random())->create()
                );
            }
        });
        Comment::all()->each(function ($comment) use ($user) {
            for ($x = 0; $x < rand(1, 50); $x++)
                $comment->like($user->random()->id);
        });
    }
}
