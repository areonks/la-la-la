<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\ConversationController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::get('tags/{tag:name}/posts', [PostController::class, 'getAllByTag']);

    Route::group(['prefix' => '/posts'], function () {
        Route::post('', [PostController::class, 'store']);
        Route::get('/{post}', [PostController::class, 'show']);
        Route::put('/{post}', [PostController::class, 'update'])->middleware('can:update,post');
        Route::delete('/{post}', [PostController::class, 'destroy'])->middleware('can:destroy,post');
        Route::post('/{post}/like', [PostController::class, 'addLike']);
        Route::post('/{post}/unlike', [PostController::class, 'removeLike']);


        Route::group(['prefix' => '/{post}/comments'], function () {
            Route::get('', [CommentController::class, 'index']);
            Route::post('', [CommentController::class, 'store']);
        });
    });

    Route::group(['prefix' => '/comments'], function () {
        Route::put('/{comment}', [CommentController::class, 'update'])->middleware('can:update,comment');
        Route::delete('/{comment}', [CommentController::class, 'destroy'])->middleware('can:destroy,comment');
        Route::post('/{comment}/like', [CommentController::class, 'addLike']);
        Route::post('/{comment}/unlike', [CommentController::class, 'removeLike']);

    });

    Route::post('/{user:id}/follow', [UserController::class, 'follow']);
    Route::post('/{user:id}/unfollow', [UserController::class, 'unfollow']);

    Route::group(['prefix' => '/conversations'], function () {
        Route::post('', [ConversationController::class, 'store']);
        Route::post('/{conversation:id}', [ConversationController::class, 'sendMessage']);
        Route::get('/{conversation:id}/messages', [ConversationController::class, 'showMessages']);
    });

    Route::group(['prefix' => 'users'], function () {
        Route::get('', [UserController::class, 'search']);
        Route::get('{user:id}/followings', [UserController::class, 'getFollowings']);
        Route::get('followings/posts', [PostController::class, 'getAllFollowings']);
        Route::get('{user:id}/followers', [UserController::class, 'getFollowers']);

        Route::group(['prefix' => '/{user:username}'], function () {
            Route::get('/posts', [PostController::class, 'index']);
            Route::get('', [UserController::class, 'show']);

        });
    });

    Route::group(['prefix' => 'profile'], function () {

        Route::put('', [ProfileController::class, 'update']);
        Route::get('', [ProfileController::class, 'show']);

    });

});


//
