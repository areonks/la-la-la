<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed user
 */
class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'content' => $this->content,
            'author' => new UserResource($this->user),
            'mentionedUsers' => UserResource::collection($this->mentionedUsers),
            'tags' => TagResource::collection($this->tags),
            'likes' => $this->liked_users_count,
            'isLiked' => $this->is_liked,
        ];
    }
}
