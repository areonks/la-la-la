<?php

namespace App\Http\Controllers;

use App\Events\MessageEvent;
use App\Http\Resources\MessageResource;
use App\Models\Conversation;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ConversationController extends Controller
{
    public function index(User $user)
    {
        $messages = Auth::user()->conversation($user->id)->orderBy('created_at', 'DESC')->cursorPaginate(5);
        return MessageResource::collection($messages);
    }

    public function store(Request $request, User $user)
    {

        $test = Auth::user()->conversationUsers()->conversation()->

        dd($test);
        $message = $user->conversationMessages()->save($message->validated());

        return new MessageResource($message);
    }

//    public function store(Request $request, User $user)
//    {
//
//
//
//        dd($message);
//        $message = $user->conversationMessages()->save($message->validated());
//        event(new MessageEvent($message, $user->username));
//
//        return new MessageResource($message);
//    }
}
