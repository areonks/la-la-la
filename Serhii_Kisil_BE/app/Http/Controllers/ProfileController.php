<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateProfileRequest;
use App\Http\Resources\ProfileResource;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function show()
    {
        $user = Auth::user();
        return new ProfileResource($user->profile);
    }

    public function update(UpdateProfileRequest $request)
    {
        $profile = $request->user()->profile;

        if ($request->hasFile('avatar')) {
            $request->user()->profile->updateAvatar($request->avatar);
        }

        $request->user()->profile->update($request->only(['name']));

        return new ProfileResource($profile);
    }

}
