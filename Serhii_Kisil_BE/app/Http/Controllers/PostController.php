<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index(User $user)
    {
        return PostResource::collection($user->posts()->orderBy('id', 'DESC')->cursorPaginate(5));
    }

    public function show(Post $post)
    {
        return new PostResource($post);
    }

    public function addLike(Post $post, Request $request)
    {
        $post->like($request->user()->id);
        return response()->noContent();
    }

    public function removeLike(Post $post, Request $request)
    {
        $post->unLike($request->user()->id);
        return response()->noContent();
    }

    public function getAllByTag(Tag $tag)
    {
        return PostResource::collection(Post::whereHasTag($tag->name)->orderBy('id', 'DESC')->cursorPaginate(10));
    }

    public function getAllFollowings(Request $request)
    {
        return PostResource::collection($request->user()->followingsPosts()->orderBy('id', 'DESC')->cursorPaginate(10));
    }

    public function store(StorePostRequest $request)
    {
        $post = $request->user()->posts()->create($request->validated());
        return new PostResource($post);
    }

    public function update(UpdatePostRequest $request, Post $post)
    {
        $post->update($request->validated());
        return new PostResource($post);
    }

    public function destroy(Post $post)
    {
        $post->delete();
        return response()->noContent();
    }

}
