<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Http\Resources\CommentResource;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller

{
    public function index(Post $post)
    {
        return CommentResource::collection($post->comments()->cursorPaginate(10));
    }

    public function addLike(Comment $comment, Request $request)
    {
        $comment->like($request->user()->id);
        return response()->noContent();
    }

    public function removeLike(Comment $comment, Request $request)
    {
        $comment->unLike($request->user()->id);
        return response()->noContent();
    }

    public function store(StoreCommentRequest $request, Post $post)
    {
        $comment = Auth::user()->comments()->make($request->validated());
        $comment = $post->comments()->save($comment);
        return new CommentResource($comment);
    }

    public function update(UpdateCommentRequest $request, Comment $comment)
    {
        $comment->update($request->validated());
        return new CommentResource($comment);
    }

    public function destroy(Comment $comment)
    {
        $comment->delete();
        return response()->noContent();
    }

}

