<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function search(Request $request)
    {
        return UserResource::collection(User::whereUserName($request->username)->paginate(3));
    }

    public function show(User $user)
    {
        return new UserResource($user);
    }

    public function follow(User $user)
    {
        Auth::user()->follow($user->id);
        return response()->noContent();
    }

    public function unfollow(User $user)
    {
        Auth::user()->unfollow($user->id);
        return response()->noContent();
    }

    public function getFollowers(User $user)
    {
        return UserResource::collection($user->followers);
    }

    public function getFollowings(User $user)
    {
        return UserResource::collection($user->followings);
    }
}
