<?php

namespace App\Models;

use App\Traits\HasLikes;
use App\Traits\HasMentions;
use App\Traits\HasTags;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed user_id
 * @property mixed post_id
 */
class Comment extends Model
{
    use HasFactory, HasTags, HasMentions, HasLikes;

    protected $fillable = [
        'content'
    ];

    protected $withCount = ['likedUsers'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }

}
