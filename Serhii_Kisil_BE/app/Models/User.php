<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property mixed id
 */
class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeWhereUserName(Builder $builder, $query)
    {
        return $query ? $builder->where('username', 'LIKE', "%$query%") : $builder;
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);

    }

    public function conversationMessages()
    {
        return $this->hasMany(ConversationMessage::class);
    }

    public function conversationUsers()
    {
        return $this->hasMany(ConversationUser::class);
    }

    public function followings()
    {
        return $this->belongsToMany(User::class, 'followings', 'follower_id', 'user_id');
    }

    public function followers()
    {
        return $this->belongsToMany(User::class, 'followings', 'user_id', 'follower_id');
    }

    public function followingsPosts()
    {
        return $this->belongsToMany(Post::class, 'followings', 'follower_id', 'user_id', null, 'user_id');
    }

    public function follow($userId)
    {
        $this->followings()->syncWithoutDetaching($userId);
    }

    public function unfollow($userId)
    {
        $this->followings()->detach($userId);
    }

    public function scopeConversation($query, $recipientId)
    {
        return $recipientId ?
            $this->messagesSend()->where('recipient_id', '=', $recipientId)
            : $query;
    }
}
