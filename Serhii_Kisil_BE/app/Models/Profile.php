<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Profile extends Model
{
    use HasFactory;


    protected $fillable = [
        'name',
        'avatar'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getAvatarUrlAttribute()
    {
        return $this->attributes['avatar'] ? asset('storage/' . $this->attributes['avatar']) : null;
    }

    public function updateAvatar($avatar)
    {
        Storage::delete('public/' . $this->avatar);
        $this->update(['avatar' => $avatar->store('images', 'public')]);
    }
}
