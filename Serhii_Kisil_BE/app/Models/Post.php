<?php

namespace App\Models;

use App\Traits\HasLikes;
use App\Traits\HasMentions;
use App\Traits\HasTags;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed user_id
 * @property mixed id
 */
class Post extends Model
{
    use HasFactory, HasTags, HasMentions, HasLikes;

    protected $fillable = [
        'content'
    ];

    protected $withCount = ['likedUsers'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function scopeWhereHasTag($query, $tagName)
    {
        return $tagName ? $query->whereHas('comments', function (Builder $query) use ($tagName) {
            $query->whereHas('tags', function (Builder $query) use ($tagName) {
                $query->where('name', '=', $tagName);
            });

        })->orWhereHas('tags', function (Builder $query) use ($tagName) {
            $query->where('name', '=', $tagName);
        }) : $query;
    }

}
