<?php

namespace App\Observers;

use App\Models\Comment;

class CommentObserver
{
    /**
     * Handle the Comment "created" event.
     *
     * @param \App\Models\Comment $comment
     * @return void
     */
    public function created(Comment $comment)
    {
        $comment->parseTags();
        $comment->parseMentions();
    }

    /**
     * Handle the Comment "updated" event.
     *
     * @param \App\Models\Comment $comment
     * @return void
     */
    public function updated(Comment $comment)
    {
        $comment->parseTags();
        $comment->parseMentions();
    }

    public function deleted(Comment $comment)
    {
        $comment->tags()->detach();
        $comment->mentionedUsers()->detach();
        $comment->likedUsers()->detach();

    }
}


