<?php

namespace App\Observers;

use App\Models\Post;

class PostObserver
{
    /**
     * Handle the Post "created" event.
     *
     * @param \App\Models\Post $post
     * @return void
     */
    public function created(Post $post)
    {
        $post->parseTags();
        $post->parseMentions();
    }

    /**
     * Handle the Post "updated" event.
     *
     * @param \App\Models\Post $post
     * @return void
     */
    public function updated(Post $post)
    {
        $post->parseTags();
        $post->parseMentions();

    }

    public function deleted(Post $post)
    {
        $post->tags()->detach();
        $post->mentionedUsers()->detach();
        $post->likedUsers()->detach();

    }

}
