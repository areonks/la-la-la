<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class MentionEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $item;
    public $username;

    public function __construct($item, $username)
    {
        $this->item = $item;
        $this->username = $username;
    }

    public function broadcastOn()
    {
        return new Channel($this->username);
    }

    public function broadcastAs()
    {
        return 'mentionEvent';
    }
}
