<?php

namespace App\Traits;

use App\Events\MentionEvent;
use App\Models\User;

trait HasMentions
{
    public function mentionedUsers()
    {
        return $this->morphToMany(User::class, 'mentionable');
    }

    public function parseMentions()
    {
        {
            preg_match_all("/@([\p{Pc}\p{N}\p{L}\p{Mn}\p{P}]+)/u", $this->content, $matches);
            $mentions = [];

            foreach (array_unique($matches[1]) as $mention) {
                $user = User::where('username', $mention)->first();
                if ($user) {
                    event(new MentionEvent($this, $user->username));
                    array_push($mentions, $user->id);
                }

            }

            $this->mentionedUsers()->sync($mentions);

        }
    }
}

