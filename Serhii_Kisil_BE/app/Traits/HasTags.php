<?php

namespace App\Traits;

use App\Models\Tag;

trait HasTags
{
    public function parseTags()
    {
        {
            preg_match_all("/#(\w+)/", $this->content, $matches);
            $tags = [];

            foreach (array_unique($matches[1]) as $tag) {
                array_push($tags, Tag::firstOrCreate(['name' => $tag])->id);
            }

            $this->tags()->sync($tags);
        }
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }
}
